# **3DPRINTING**

## Step1. **Files** from CAD

[File place](https://drive.google.com/drive/folders/11tem1oM93GF78-MYD0-Wly-c_m-G_X86)

![](https://gitlab.com/cielpic/picbed/uploads/fa95b2cde7af4d17f6e145391ed038b7/20200818092149.png)

## Step2. **Setting** the files in CURA

[CURA](https://ultimaker.com/software/ultimaker-cura)

### Confirm the place of the parts
![](https://gitlab.com/cielpic/picbed/uploads/02cde77017ecb87412e4fc721a07b7dc/20200818092607.png)

### Confirm the wall thickness
![](https://gitlab.com/cielpic/picbed/uploads/2f1956e663463457cf1f3efd19b7cdd5/20200818092838.png)

### Confirm the way of infill
![](https://gitlab.com/cielpic/picbed/uploads/93a4d0fad23e563dbe514b871be0de5f/20200818101048.png)

### Confirm the support placement
![](https://gitlab.com/cielpic/picbed/uploads/a7ead6a4ef065e60992cc47fa2c62f68/20200818101425.png)

## Step3. Review Slide & *Export* 3D files

![](https://gitlab.com/cielpic/picbed/uploads/38028c794ac542b7d333b2513b115f90/20200825135602.png)

![](https://gitlab.com/cielpic/picbed/uploads/ffa77450479ca749d48979365a4875b1/8b292dac76dd50e9d0cc0ad9d9ddeaa.png)

![](https://gitlab.com/cielpic/picbed/uploads/191624df7bc9b33824ca594d717b86cd/20200818101743.png)

## Step4. Setting the 3D printer

 ### Incomplete defective goods

 ![](https://gitlab.com/cielpic/picbed/uploads/2487fdcde4ee40f38f8d11f7137b8380/7511904c91e94a8a2816670807abf8f.jpg)
 This is because of using the smooth tray for 3D printing, and the parts were not fixed on the plate during the printing.
 During the printing, we need to confirm the tray of the 3D printer is rough to make sure the parts can adhere to the tray that to fix the part.

 ### Confirm the tray of 3D printer and fix it on the 3D printer
 ![](https://gitlab.com/cielpic/picbed/uploads/e7bf9bbc3c5d291735b6b169a20a386a/4783a8d2c4f122da9fff0afada98fb2.jpg)
 ![](https://gitlab.com/cielpic/picbed/uploads/4e11aff1500d021fd5caf1e8d4a5b27a/20200818104210.png)
 
 #### The videos for these seting
<iframe width="560" height="315" src="https://www.youtube.com/embed/8exExDxCfdE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

https://www.youtube.com/watch?v=ljCm2cSy9ms
<iframe width="560" height="315" src="https://www.youtube.com/embed/ljCm2cSy9ms" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/8--DMjS2_Y8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

 ### The finished parts

 ![](https://gitlab.com/cielpic/picbed/uploads/d9d36ed3e1793a2c405462101e99c812/bbcbd1af5d1089eccd224ed275ada9d.jpg)

![](https://gitlab.com/cielpic/picbed/uploads/f91f0008c80f2057eff4d7c46ac1cb71/079990cdcf55e8bb1eb401af242091e.jpg)

### Grind the product
![](https://gitlab.com/cielpic/picbed/uploads/e5cf59cb99a257166362f4776c7eddbf/4031f8bb3fa1bea2fa1d1a60c7596f5.jpg)
![](https://gitlab.com/cielpic/picbed/uploads/348bf33f091bfad385a146b5fe64c74e/1fdac3c91c783d63fdf685d7dd0dd81.jpg)
![](https://gitlab.com/cielpic/picbed/uploads/7f509e2eb52073f5a5e256b87df11dab/b3354143c619df558545641ae8a6336.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/8B-xUfc0UPU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## The cap
![](https://gitlab.com/cielpic/picbed/uploads/22b9522effc2154043a2500868ff2c1c/15deaa2884b3d7ba3a4fdd68550ff81.jpg)

![](https://gitlab.com/cielpic/picbed/uploads/ffa1d12a4eca8d9bc990e37143ac59ba/302c6f37a706668625e87e05ba62e18.jpg)
