# 杯具组成员（Jenny & Ciel )
# [Jenny](https://nex-fab.gitlab.io/fab-02/jenny/1projectmanagement/introduce.html)

# Final Project -**A cup with LCD** 

## Aim
A cup with an LCD that can display the actual temperature, and can heat the temperature to the aimed level (55 ℃).

## Tech analysis
### EE
### ME
### Code
### Material
### IOT(Ali Cloud)

# 初步讨论 2020-09-04
## 功能
### Input
1. 测温 （所需温度传感器）
2. 检测喝水次数 （加速度传感器）
3. 检测喝水量 （水位传感器，或者光敏传感器）

### Output
1. 温度控制（继电器-弱电控制强电）
2. 显示屏或者的LED灯（内容展示）

### IOT 
1. 温度上传（Ali Cloud）
2. 电脑远程控制开关（温度调节，或者是添加语音提示）

## 设计
### 电子

### 结构
Solidworks, AutoCAD, 3D printing, Lasercutting
### 代码

