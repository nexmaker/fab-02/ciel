# Introduction

## Experience

Hi, this is Ciel.

Now I'm an onboarding manager in NexPCB, my main job is to help our customers to finish their project with our capabilities.

![](https://gitlab.com/cielpic/picbed/uploads/0ee92c6083896718ee6e4ba7646b0339/logo-small__2_.png)

The work is started from Nov 2018, which means I worked for NexPCB is around 2 years. This experience is very valuable for me.

My hope is that I can find my own value in the work and improve myself ASAP.

![](https://gitlab.com/cielpic/picbed/uploads/6540d5a9fa4c81216641e2fa60188424/20200730164123.png)

## Company

 Welcome to NexPCB, (https://www.nexpcb.com).
Lab in  [NexPCB](https://nexpcb.com).

## LinkedIn

[Ciel](https://www.linkedin.com/in/ciel-luo-30557a1a1/)
