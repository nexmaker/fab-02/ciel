# AutoCAD
## Keyboard Shortcuts

1. L: Line
2. C: Circle
3. REC: Rectangle
4. Offset: Expansion
5. CO: Copy
6. M: Move
7. Lay: Layer
8. MA: Format Painter 
9. TEXT: Text
10. MI: Mirroring
11. TR: Delete
12. RO：Rotate

## Drawing on AutoCAD

### Create Layer
(Name the layers, choose the type of lines, update the color of lines, )
![](https://gitlab.com/cielpic/picbed/uploads/e8680ce3fb8c1602bf4255f6c12aa3cc/9e66ec9d37abff5b641b2852641a1f4.png)
![](https://gitlab.com/cielpic/picbed/uploads/a4d442d7635caa3a9249fb0da42b624d/f9bee6aee5525f64128847aa45edc0b.png)
![](https://gitlab.com/cielpic/picbed/uploads/52404d48ed9a5dfdd805812a6ed25464/3ff4b0fb7215e540b469844db2bbeab.png)
![](https://gitlab.com/cielpic/picbed/uploads/de0f8507a574da58814be37b8a3274ab/1b3ce34f6d8cd456c05d41e7fdd8767.png)

### Draw the boards
Use the commend of "REC" to draw a rectangle, and use "Offset" to expanded it.
![](https://gitlab.com/cielpic/picbed/uploads/22ca5664e29a307fcb8315ad996d98d7/ddbc8b908b08bbb4adc1828d7927566.png)

In the centre layer to draw the centre lines.
![](https://gitlab.com/cielpic/picbed/uploads/451d5cf40441bf7dc8699ff076cce04b/94b473d778d6b6fea109388b66a8b5a.png)

Use "Offset" to make two lines and parallel the center line. (15mm) 
![](https://gitlab.com/cielpic/picbed/uploads/75ea3c8e5cc4b49e920025333ec9a197/6471775ea5bddbeaa4328187ba03eff.png)

Use "TR" to delete the excess lines
![](https://gitlab.com/cielpic/picbed/uploads/de3bdd162ec2cf8743b75e789eb2f832/2d39e3752b5aa0d8dd17f84c0d55ff7.png)

Use "MA" to Format Painter the lines
![](https://gitlab.com/cielpic/picbed/uploads/41702bf2ede9d6d0177a7a398833c306/5a8ee18e8c94f12d84c9bcfc16925ac.png)

Use "MI" mirring the lines
![](https://gitlab.com/cielpic/picbed/uploads/c8f136139087b7b5242e485bd25ab740/d5a7dc7e9135b376d307d066314e4a6.png)

Copy the board and name every boards
![](https://gitlab.com/cielpic/picbed/uploads/7089e9f8ce07e4fbe9a2b1a119b8fdd0/437216c826ccbdfde224ba63b5f9c53.png)

### Files [Laser cutting](https://drive.google.com/drive/folders/175vrnA5TTUkJgfHcEXBas-qNCoDBZAbu)

Draw the picture
![](https://gitlab.com/cielpic/picbed/uploads/999080254e0767524b94d20365f241d0/b617cc87e40f90031207b5e01d681b2.png)

### Export ["DXF" files](https://drive.google.com/drive/folders/175vrnA5TTUkJgfHcEXBas-qNCoDBZAbu)
![](https://gitlab.com/cielpic/picbed/uploads/6e473f9ec37079f4e74adb4c5c85dddd/b80aeb43ebb88bb2ad661d05f91b5c1.png)

## Setting Laser cutting
1. 确定打印起始点，确认切割点和板子之间的距离

![](https://gitlab.com/cielpic/picbed/uploads/2925cbfd479ec883a22774f1129c680f/fc28c7258c81cf3792996ff3c34dcb7.jpg)

![](https://gitlab.com/cielpic/picbed/uploads/10e28bc135002fb1a6f300922f60f03f/bb87601ceb249f7e44c320fcce816f9.jpg)

2. 控制面板（移动按键，还有打印框范围确认）

![](https://gitlab.com/cielpic/picbed/uploads/3ec9477f7a4c67b7022cc630b247a978/d163791552878ed102a101904a6512c.jpg)

3. 确认切割速度和功率

![](https://gitlab.com/cielpic/picbed/uploads/f52cce394313a9f87027f8b0b98755a5/62e1136073803799063a6bbe7b3c539.png)

4. 调整切割的顺序：一般是先做雕刻图案，再打孔，最后做切割块。

![](https://gitlab.com/cielpic/picbed/uploads/b3aecc52982d84f04300d97e27344ae4/20200902144036.png)

5. “加载”项，确认文件名，再“下传文件”到切割机，一定要确认是联机状态，要不然会出现第二张出现的问题

![](https://gitlab.com/cielpic/picbed/uploads/adb8376c8730d50997e014ae740f7baf/c4bb57eabacff2f5d66b866034d4752.png)
![](https://gitlab.com/cielpic/picbed/uploads/10474e70ec22a6c37124a62d60470e39/20200902144300.png)

## Assemble all the panel

![](https://gitlab.com/cielpic/picbed/uploads/386533f475966898e95966b00385c654/ac211e80f2b88b7347180c7d1f9b8a7.jpg)

![](https://gitlab.com/cielpic/picbed/uploads/be2cab4387f3b29c44227354a5adce9b/35ec17c1d39f7946a83563efc04d33c.jpg)

![](https://gitlab.com/cielpic/picbed/uploads/552922ae02e58a1a443eb60e42784b75/f6807de4ae0223d7c5453d4304b2b4d.jpg)

## Video for the processing

<iframe width="560" height="315" src="https://www.youtube.com/embed/ISq34ubK0LU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Main issues
1. Unmatch
由于没有考虑到匹配度，导致切出来有两个面不能对接上
 ![](https://gitlab.com/cielpic/picbed/uploads/9d6c2324ff8d7540620f406db3fcad13/35a9aa1b4d597f32ce2e7916daefb34.jpg)

 调整两个面的尺寸，缩小2.4mm的宽度，最终匹配合适
![](https://gitlab.com/cielpic/picbed/uploads/0784643d4aba0795fe44d45feab6fb5f/64b543a000541e8da0357eb9f781cc6.jpg)

2. [Kerf](https://www.nexmaker.com/doc/6laser_cutter/Design_guide.html
)
没有考虑到激光的半径，所以最终组装的时候不是紧密结合的，需要借助胶水。这里就有一个Kerf的问题，
设置了10个5mm宽的长条测试，
![](https://gitlab.com/cielpic/picbed/uploads/f770f572f0b12f0cfec04c725b3a0d32/e8fa17bece97ebdb859d3ebf6f0582c.jpg)
实际打出来的宽度是47.88mm
![](https://gitlab.com/cielpic/picbed/uploads/166a263c1b960c7a94687879a61b7544/274634cf9e6a8ac24387f493631fdbb.jpg)

公式：kerf= (50-47.88)/10=0.212mm