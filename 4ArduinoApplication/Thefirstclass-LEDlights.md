# LED light Water Control

## Main points:
1. LED lights: LED light: long leg is Positive poles
2. GND is Negative poles
3. [Arduino](https://www.arduino.cc/en/Main/Donate)
4. [Tinkercad](https://www.tinkercad.com/things/7W503tOhTMo-super-uusam-jaagub/editel)

## Arduino file
![](https://gitlab.com/cielpic/picbed/uploads/bf743deaafce42cbddde7b1d6110879c/9fa610a725a84c6771126cf1c998a5a.png)

```
void setup() {
pinMode(13, OUTPUT);
pinMode(12, OUTPUT);
pinMode(11, OUTPUT);
}

void loop() {
digitalWrite(13, HIGH);
digitalWrite(12, LOW);
digitalWrite(11, LOW);
delay(1000); //Wait for 1000 millisecond(s)
  
digitalWrite(13, LOW);
digitalWrite(12, HIGH);
digitalWrite(11, LOW);
delay(1000); //Wait for 1000 millisecond(s)

digitalWrite(13, LOW);
digitalWrite(12, LOW);
digitalWrite(11, HIGH);
delay(1000); //Wait for 1000 millisecond(s)

digitalWrite(13, HIGH);
digitalWrite(12, LOW);
digitalWrite(11, HIGH);
delay(200); //Wait for 200 millisecond(s)

digitalWrite(13, HIGH);
digitalWrite(12, LOW);
digitalWrite(11, LOW);
delay(1000); //Wait for 1000 millisecond(s)

digitalWrite(13, HIGH);
digitalWrite(12, HIGH);
digitalWrite(11, LOW);
delay(2000); //Wait for 2000 millisecond(s)
  
}
```

## Presentation
<iframe width="560" height="315" src="https://www.youtube.com/embed/45BTmK8hff0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>