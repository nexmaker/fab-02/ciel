# 感光灯

## 项目介绍

我们这次做的这个非常好玩，叫做感光灯。它能随着光线明暗而选择是否亮灯。这个光感灯非常适合用做夜晚使用的小夜灯。晚上睡觉的时候，家中灯关掉后，感光灯感觉到周围环境变暗了，就自动亮起。到了白天，天亮后，感光灯就又恢复到关闭的状态了

## 项目材料

* 3×  5mm LED灯
* 3×  220欧电阻
* 1×  10k电阻
* 1×  光敏二极管 

## [项目参考](https://mc.dfrobot.com.cn/thread-2505-1-1.html)

在参考项目的基础上增加了两个LED灯，两个电阻，做成条件不同灯光显示不同的效果


## 硬件连接 [Tinkercad](https://www.tinkercad.com/things/2dxXrCW80Pw-brilliant-elzing-amberis/editel?tenant=circuits)
![](https://gitlab.com/cielpic/picbed/uploads/7f6e1f55635de4610f43865653efd552/e92aa9c0f9d4c63d1278d7f8f74b11d.png)
![](https://gitlab.com/cielpic/picbed/uploads/602a6269cf43de3a9b689a8b1c541fc5/b46fa0a1bc65be2be6cf9702f51ce53.png)
![](https://gitlab.com/cielpic/picbed/uploads/aa49cafa4d1ebb5710e0cba95ff1889c/775244bedd1a8451200267832b27fc5.jpg)

## 软件书写
```
int LED1 = 13;                     //设置LED灯为数字引脚13
int LED2 = 12;                     //设置LED灯为数字引脚13
int LED3 = 11;                     //设置LED灯为数字引脚13
int val = 0;                      //设置模拟引脚0读取光敏二极管的电压值

void setup(){
      pinMode(LED1,OUTPUT); 
      pinMode(LED2,OUTPUT);
      pinMode(LED3,OUTPUT);// LED为输出模式
      Serial.begin(9600);        // 串口波特率设置为9600
}

void loop(){
      val = analogRead(0);         // 读取电压值0~1023
      Serial.println(val);         // 串口查看电压值的变化
      if(val<200){                // 一旦小于设定的值，LED灯关闭
              digitalWrite(LED1,LOW);
              digitalWrite(LED2,HIGH);
              digitalWrite(LED1,LOW);
      }else if(val>200 & val<700){                        // 否则LED亮起
              digitalWrite(LED1,HIGH);
              digitalWrite(LED2,LOW);
              digitalWrite(LED3,LOW);
      }else{                        // 否则LED亮起
              digitalWrite(LED1,HIGH);
              digitalWrite(LED2,HIGH);
              digitalWrite(LED3,HIGH);
      delay(10);                   // 延时10ms
      }
}
```
### Verify the program

![](https://gitlab.com/cielpic/picbed/uploads/05556003241b223b765fec8fa654d468/9f5280e6d80e448a0ec8dac3493cab5.png)

![](https://gitlab.com/cielpic/picbed/uploads/773d37a08684e6fbe7d62d912740c860/00f69bec142cfdd220e5f6917ace76f.png)

### Confirm the port

![](https://gitlab.com/cielpic/picbed/uploads/8af7ca17627a424b70e915400be5135c/cdc399edd91ff1fa045d2205c567bc5.jpg)

### Upload program

![](https://gitlab.com/cielpic/picbed/uploads/ae9b72a6a8a388a0916f296b0ac9a15d/fcf0add30c36bcacf2b95aa14cadc88.png)

### Serial port monitoring

![](https://gitlab.com/cielpic/picbed/uploads/adbad1e5bf3f34467563cd440fa2fb1b/0bb6251505247bec3614121f4ad7e67.png)

### Run the program

<iframe width="560" height="315" src="https://www.youtube.com/embed/JgXN_ZFGcm8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>